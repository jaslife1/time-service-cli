build:
	docker build -t time_service_cli .

run:
	docker run \
		-e MICRO_REGISTRY=mdns \
		time_service_cli